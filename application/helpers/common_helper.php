<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function public_url($url = ''){
    return base_url('public/'.$url);
}
function pre($list, $exit = ''){
    echo '<pre>';
    print_r($list);
    echo '</pre>';
    if($exit){
        die();
    }
}
function desc($content){
    $content = nl2br($content);
    $content = strip_tags($content);
    $content = substr($content,0, 160);
    $content = trim($content);

    return $content;
}
// Chuc nang khong dau

function get_date($time, $full_time = true){
    $fomat = '%d-%m-%Y';
    if($full_time){
        $fomat = $fomat.' - %H:%i:%s';
    }
    $date = mdate($fomat, $time);
    return $date;
}
function get_day($time){
    $fomat = '%H:%i %d-%m-%Y';
    $date = mdate($fomat, $time);
    return $date;
}

function curl($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_2_1 like Mac OS X) AppleWebKit/602.4.6 (KHTML, like Gecko) Version/10.0 Mobile/14D27 Safari/602.1');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $result = curl_exec($ch);
    if ($result) {
        return $result;
    } else {
        return curl_error($ch);
    }
    curl_close($ch);
}
