<?php
class My_Controller extends CI_Controller{

    public $data = array();

    function __construct(){
        //ke thua tu CI_Controller
        parent:: __construct();

    }

    private function _check_login() {
        $controller = $this->uri->rsegment('1');
        $controller = strtolower($controller);

        $login = $this->session->userdata('login');
        //neu ma chua dang nhap,ma truy cap 1 controller khac login
        if (!$login && $controller != 'login') {
            redirect(base_url('login'));
        }
        //neu ma admin da dang nhap thi khong cho phep vao trang login nua.
        if ($login && $controller == 'login') {
            redirect(base_url('home'));
        }
    }
}