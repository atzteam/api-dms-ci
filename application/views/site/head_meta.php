<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title>Auto Cảm xúc</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" /><!-- text fonts -->
<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/css/fonts.googleapis.com.css" />
<!-- ace styles -->
<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/css/jquery.gritter.min.css" />
<script src="<?php echo public_url('site/') ?>assets/js/ace-extra.min.js"></script>

<link rel="stylesheet" href="<?php echo public_url('site/') ?>assets/css/select2.min.css" />
 
<script src="<?php echo public_url('site/') ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo public_url('site/') ?>assets/js/custom-function.js"></script>