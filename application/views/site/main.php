<!DOCTYPE html>
<html lang="vi">
    <head>
        <?php $this->load->view('site/head_meta'); ?>
    </head>
    <body class="no-skin">
        <?php $this->load->view('site/header'); ?>

        <div class="main-container ace-save-state" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.loadState('main-container')
                } catch (e) {
                }
            </script>

            <?php $this->load->view('site/sidebar'); ?>

            <div class="main-content">
                <div class="main-content-inner">
                    <?php $this->load->view($temp, $this->data) ?>

                </div>
            </div>

        </div>

        <?php $this->load->view('site/footer'); ?>
    </body>
</html>

