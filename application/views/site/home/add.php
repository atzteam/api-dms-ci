<div class="widget-box widget-color-dark light-border ui-sortable-handle">
    <div class="widget-header">
        <h5 class="widget-title smaller">Thêm mới</h5>
    </div>
    <div class="widget-body">
        <div class="widget-main padding-6">
            <div class="form-group" id="title_group">
                <div class="form-group" id="add_group_name">
                    <label class="control-label no-padding-right"><strong>Access Token <span>*</span> </strong></label>
                    <input type="text" placeholder="Nhập Access Token" class="form-control" name="access_token" id="access_token">
                </div>
                <div class="form-group" id="link_group">
                    <label class="control-label no-padding-right"><strong>Họ tên <span>*</span> </strong></label>
                    <input type="text" placeholder="Họ tên" class="form-control" name="name" id="name" readonly="">
                </div>
                <div class="form-group" id="sort_group">
                    <label class="control-label no-padding-right"><strong>Facebook ID</strong></label>
                    <input  type="text" placeholder="Facebook ID" class="form-control" name="fb_id" id="fb_id" readonly="">
                </div>
                <div class="form-group">
                    <label><strong>Chọn cảm xúc</strong></label>
                    <select id="reaction" class="form-control">
                        <option value="LIKE" class="form-control"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</option>
                        <option value="LOVE" class="form-control">Love</option>
                        <option value="WOW" class="form-control">Wow</option>
                        <option value="HAHA" class="form-control">Haha</option>
                        <option value="SAD" class="form-control">Sad</option>
                        <option value="ANGRY" class="form-control">Angry</option>
                    </select>
                </div>
                <div class="form-group">
                    <label><strong>Ngày hết hạn</strong></label>
                    <input id="expires" placeholder="<?php echo mdate('%d-%m-%Y') ?>" type="text" class="input-sm form-control" />
                </div>
            </div>
            <!-- panel -->
            <div class="widget-toolbox padding-8 clearfix">
                <button class="btn btn-xs btn-danger pull-left" id="add_reset" >
                    <i class="ace-icon fa fa-refresh"></i>
                    <span class="bigger-110">Reset</span>
                </button>
                <button class="btn btn-xs btn-success pull-right" id="add_btn">
                    <span class="bigger-110">Thêm mới</span>
                    <i class="ace-icon fa fa-check icon-on-right"></i>
                </button>
            </div>
        </div>
        <!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->

<script>
    $("#access_token").change(function(event) {
        var token = $("#access_token").val().trim();
        check_token(token);
    });

    function check_token(token){
        var url = 'https://graph.facebook.com/me?fields=id,name&access_token='+token;
        $.ajax({
            url: url,
            dataType: 'json'
        })
        .done(function(data) {
            if(data.id){
                $("#name").val(data.name);
                $("#fb_id").val(data.id);
            }
        })
        .fail(function() {
            $.gritter.add({
                title: 'Đã xảy ra lỗi',
                text: 'Access Token die',
                class_name: 'gritter-error'
            });
            return false;
        })        
    }
    $("#add_reset").click(function() {
        load_panel_add();
    });
    $("#add_btn").click(function() {
        var token = $("#access_token").val().trim();
        var name = $("#name").val().trim();
        var fb_id = $("#fb_id").val().trim();
        var reaction = $("#reaction").val();
        var expires = $("#expires").val();

        if(token == '' || expires == ''){
            $.gritter.add({
                title: 'Đã xảy ra lỗi',
                text: 'Bạn vui lòng nhập đầy đủ các giá trị',
                class_name: 'gritter-error'
            });
            return false;
        }

        $.ajax({
            url: '<?php echo base_url('home/add') ?>',
            type: 'POST',
            dataType: 'json',
            data: {token: token, name: name, fb_id: fb_id, reaction: reaction, expires:expires},
        })
        .done(function(js) {
            if(js.status == 1){
                $.gritter.add({
                    title: 'Thành công',
                    text: js.msg,
                    class_name: 'gritter-success'
                });
                load_panel_add();
                load_list();
            }
            else{
                $.gritter.add({
                    title: 'Đã xảy ra lỗi',
                    text: js.msg,
                    class_name: 'gritter-error'
                });
                return false;
            }
        })
        .fail(function() {
            $.gritter.add({
                title: 'Đã xảy ra lỗi',
                text: 'Không thể kết nối tới máy chủ',
                class_name: 'gritter-error'
            });
            return false;
        })
        
        
    });


</script>