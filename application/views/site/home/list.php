<div class="widget-box widget-color-dark light-border ui-sortable-handle">
    <div class="widget-header">
        <h5 class="widget-title smaller">Thống kê người dùng</h5>
    </div>
    <div class="widget-body">
        <div class="widget-main padding-6">
            <table id="simple-table" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="hidden-xs">ID</th>
                        <th>Họ tên</th>
                        <th>Facebook ID</th>
                        <th>Reaction</th>
                        <th class="hidden-xs">Ngày thêm</th>
                        <th>Ngày hết hạn</th>
                        <th class="center">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($list as $row): ?>
                        <tr class="row_<?php echo $row->id ?>">
                            <td class="text-center hidden-xs"><?php echo $row->id; ?></td>
                            <td><?php echo $row->name; ?></td>
                            <td><?php echo $row->fb_id; ?></td>
                            <td><?php echo $row->reaction; ?></td>
                            <td class="text-center hidden-xs"><?php echo mdate('%d-%m-%Y', $row->created) ?></td>
                            <td class="text-center"><?php echo mdate('%d-%m-%Y', $row->expires) ?></td>

                            <td class="center">
                                <div class="action-buttons">
                                    <a class="red" href="#" id="delete_<?php echo $row->id ?>" data-original-title="Xóa" data-toggle="tooltip" data-placement="top" class="btn btn-xs btn-danger">
                                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <script>
                            $(document).ready(function () {
                //xoa 1
                $("#delete_<?php echo $row->id ?>").click(function () {
                    var id = "<?php echo $row->id ?>";
                    $.ajax({
                        url: "<?php echo base_url("home/del") ?>",
                        type: "POST",
                        data: "id=" + id,
                        success: function () {
                            $.gritter.add({
                                title: "Thành công",
                                text: "Bạn đã xóa thành công.",
                                class_name: "gritter-success"
                            });

                            load_list();
                        }
                    })
                    return false;
                });
            });
        </script>
    <?php endforeach; ?>
</tbody>
</table>
</div>
<!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
                    </div><!-- /.row -->