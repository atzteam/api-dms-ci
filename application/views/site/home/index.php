<div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="#">Trang chủ</a>
            </li>
            <li class="active">Tổng quan</li>
        </ul><!-- /.breadcrumb -->
    </div>
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="col-md-8">
                    <div id="load_list"></div>
                </div>
                <!-- panel thêm mới -->
                <div class="col-md-4">
                    <div id="load_panel_add"></div>
                </div>
            </div><!-- /.page-content -->
        </div>

<script>
    function load_list(){
        $.ajax({
            url: "<?php echo base_url('home/load_list') ?>",
            type: "GET",
            success: function (data) {
                $('#load_list').html(data);
            }
        })
    }
    load_list();

    function load_panel_add(){
        $.ajax({
            url: "<?php echo base_url('home/load_panel_add') ?>",
            type: "GET",
            success: function (data) {
                $('#load_panel_add').html(data);
            }
        })
    }
    load_panel_add();
</script>