<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120">
                <span class="blue bolder">AUTO REACTIONS ATZ</span>
                 © 2016-2017
            </span>
        </div>
    </div>
</div>
<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement)
        document.write("<script src='<?php echo public_url('site/assets/js/jquery.mobile.custom.min.js') ?>'>" + "<" + "/script>");
</script>
<script src="<?php echo public_url('site/assets/js/bootstrap.min.js') ?>"></script>

<!-- table -->
<script src="<?php echo public_url('site/assets/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.dataTables.bootstrap.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/dataTables.buttons.min.js') ?>"></script>
<!-- validate -->
<script src="<?php echo public_url('site/assets/js/wizard.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/bootbox.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.maskedinput.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/select2.min.js') ?>"></script>
<!-- page specific plugin scripts -->
<script src="<?php echo public_url('site/assets/js/jquery-ui.custom.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.ui.touch-punch.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.sparkline.index.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.flot.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.flot.pie.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.flot.resize.min.js') ?>"></script>
<!-- ace scripts -->
<script src="<?php echo public_url('site/assets/js/ace-elements.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/ace.min.js') ?>"></script>
<script src="<?php echo public_url('site/assets/js/jquery.gritter.min.js') ?>"></script>
<!-- inline scripts related to this page -->
<!-- <script src="<?php echo public_url('site/assets/js/custom-js.js') ?>"></script> -->
