<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Upload extends My_Controller {
    function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        $this->load->model('token_model');
    }

    function image(){

		$data = $this->input->post('img_src');
    	//$data = json_decode($data);
		
		echo $data;

		$img_src = base64_decode($data);
	
		$img_name = now();
		$file = fopen('./upload/'.$img_name.'.jpg', 'wb');
		fwrite($file, $img_src);
		fclose($file);

    }

    function test(){
    	$path = './upload/1.png';
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$data = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
		echo $base64;
    }
}
