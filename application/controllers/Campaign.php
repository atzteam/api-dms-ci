<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Campaign extends My_Controller {
    function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        $this->load->model('token_model');
        $this->load->model('campaign_model');
        $this->load->model('campaign_detail_model');
    }

    function getAll(){
    	$data = $this->input->get('data');
    	$data = json_decode($data);
    	if(!isset($data->token)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
    	//check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Token không tồn tại');
        	echo json_encode($data); return false;
        }
        
        $list = $this->campaign_model->get_list();

        foreach ($list as $row) {
        	$input['where'] = array('id_campaign' => $row->id);
        	$list_detail = $this->campaign_detail_model->get_list($input);
        	$row->detail = $list_detail;
        }

        $data = array(
        	'status' => '200',
        	'msg'	=> 'lấy thành công',
        	'list'	=> $list
        	);
        echo json_encode($data);
    }

    function getByUser(){
    	$data = $this->input->get('data');
    	$data = json_decode($data);
    	if(!isset($data->id) || !isset($data->token)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
        //check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Token không tồn tại');
        	echo json_encode($data); return false;
        }

        $info = $this->user_model->get_info($data->id);
        if(!$info){
        	$data = array('status' => '401', 'msg' => 'Không tồn tại nhân viên này');
        	echo json_encode($data); return false;
        }

        $input['where'] = array('id_user' => $info->id);
        $list_detail_user = $this->campaign_detail_model->get_list($input);
        foreach($list_detail_user as $row){
        	$row->info_campaign = $this->getCampaignByDetail($row->id_campaign);
        }
        $data = array(
        	'status'	=> '200',
        	'msg'		=> 'thành công',
        	'data'		=> $list_detail_user
        	);
        echo json_encode($data);
    }

    private function getCampaignByDetail($id = ''){
    	$info = $this->campaign_model->get_info($id);
    	return $info;
    }
}
