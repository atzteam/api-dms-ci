<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends My_Controller {
    function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        $this->load->model('token_model');
    }

    function getInfo(){
    	//truyen vao id user va token

    	$data = $this->input->get('data');
    	$data = json_decode($data);
    	if(!isset($data->id) || !isset($data->token)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
        //check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Token không tồn tại');
        	echo json_encode($data); return false;
        }

        $info = $this->user_model->get_info($data->id);
        if(!$info){
        	$data = array('status' => '401', 'msg' => 'Không tồn tại nhân viên này');
        	echo json_encode($data); return false;
        }

        $user = array(
        	'id'	=> $info->id,
        	'username'	=> $info->username,
        	'fullname'	=> $info->fullname,
        	'admin'		=> $info->admin,
        	'created'	=> $info->created,
        	);

        $data = array(
        	'status' => '200',
        	'msg'		=> 'Lấy thông tin user thành công',
        	'data'	=> $user
        	);
        echo json_encode($data);
        
    }
}
