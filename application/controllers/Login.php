<?php
Class Login extends My_Controller{
    function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        $this->load->model('token_model');
        $this->load->library('session');
    }

    function index(){
        $data = $this->input->get('data');
        if(!$data){
            $arr = array(
                'status'    => '404',
                'msg'       => 'Thiếu thông tin đăng nhập'
                );
            echo json_encode($arr); return false;
        }

        $data = json_decode($data);
        $where = array(
            'username'  => $data->username,
            'password'  => $data->password
            );
        if($this->user_model->check_exists($where)){
            $user = $this->user_model->get_info_rule($where);
            
            $token = $this->create_token();
            $info_user = array(
                'id'        => $user->id,
                'fullname'  => $user->fullname,
                'admin'     => $user->admin,
                'token'     => $token
                );
            $arr = array(
                'status'    => '200',
                'msg'       => 'Đăng nhập thành công',
                'data'      => $info_user
                );

            //xoa het token cu cua user do
            $input['where'] = array('id_user' => $user->id);
            $list_token = $this->token_model->get_list($input);
            foreach($list_token as $row){
                $this->token_model->delete($row->id);
            }
            // luu token
            $token_data = array('id_user' => $user->id, 'content' => $token, 'created' => now());
            $this->token_model->create($token_data);
            echo json_encode($arr); 
        }
        else{
            $arr = array(
                'status'    => '401',
                'msg'       => 'Thông tin đăng nhập không chính xác'
                );
            echo json_encode($arr); return false;
        }
    }

    function check_login($data = array()){

        if($this->user_model->check_exists($data)){
            return true;
        }
        
        
    }


    function create_token(){
        $time = now();
        return md5($time);
    }


}

