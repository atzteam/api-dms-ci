<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkin extends My_Controller {
    function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        $this->load->model('token_model');
        $this->load->model('campaign_model');
        $this->load->model('campaign_detail_model');
        $this->load->model('checkin_model');
    }

    function index(){
    	$data = $this->input->get('data');
    	$data = json_decode($data);
    	if(!isset($data->user_id) || !isset($data->token) || !isset($data->id_campaign) || !isset($data->content) || !isset($data->lat) || !isset($data->lng)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
        //check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Token không tồn tại');
        	echo json_encode($data); return false;
        }

        $info = $this->user_model->get_info($data->user_id);
        if(!$info){
        	$data = array('status' => '401', 'msg' => 'Không tồn tại nhân viên này');
        	echo json_encode($data); return false;
        }

        $data = array(
        	'id_user'		=> $data->user_id,
        	'id_campaign'	=> $data->id_campaign,
        	'content'		=> $data->content,
        	'lat'			=> $data->lat,
        	'lng'			=> $data->lng,
        	'created'		=> now()
        	);
        $this->checkin_model->create($data);
        $ar = array('status'	=> '200', 
        	'msg'	=> 'thành công'
        	);
        echo json_encode($ar);
   	}

   	function getLastAll(){
   		$data = $this->input->get('data');
    	$data = json_decode($data);
    	if(!isset($data->token)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
        //check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Bạn không có quyền truy cập');
        	echo json_encode($data); return false;
        }

        $checkin_array = array();
        $list_user = $this->user_model->get_list();
        foreach($list_user as $row){
			$this->db->select('max(id), id_user, lat, lng, content, created');
			$this->db->where('id_user', $row->id); 
			$query = $this->db->get('checkin');
			$checkin_array[] = $query->result();
        }
        $data = array(
        	'status'	=> '200',
        	'msg'		=> 'thành công',
        	'data'		=> $checkin_array
        	);
        echo json_encode($data);
   	}

   	function getLastByUser(){
   		$data = $this->input->get('data');
    	$data = json_decode($data);
    	if(!isset($data->id) || !isset($data->token)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
        //check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Bạn không có quyền truy cập');
        	echo json_encode($data); return false;
        }

        $info = $this->user_model->get_info($data->id);
        if(!$info){
        	$data = array('status' => '401', 'msg' => 'Không tồn tại nhân viên này');
        	echo json_encode($data); return false;
        }

        $this->db->select('max(id), id_user, lat, lng, content, created');
		$this->db->where('id_user', $data->id); 
		$query = $this->db->get('checkin');
		$last = $query->result();
        $data = array(
        	'status'	=> '200',
        	'msg'		=> 'thành công',
        	'data'		=> $last
        	);
        echo json_encode($data);
   	}
}
