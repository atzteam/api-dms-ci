<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Error extends My_Controller {
    function __construct(){
        parent:: __construct();
        $this->load->model('user_model');
        $this->load->model('token_model');
        $this->load->model('campaign_model');
        $this->load->model('campaign_detail_model');
        $this->load->model('campaign_error_model');
    }
    function index(){
    	$data = $this->input->get('data');
    	$data = json_decode($data);

    	if(!isset($data->id_user) || !isset($data->token) || !isset($data->id_detail) || !isset($data->n_house) || !isset($data->img) || !isset($data->reason)){
    		$data = array('status' => '401', 'msg' => 'Thiếu dữ liệu');
        	echo json_encode($data); return false;
    	}
        //check token
        $where = array('content' => $data->token);
        if(!$this->token_model->check_exists($where)){
        	$data = array('status' => '401', 'msg' => 'Bạn không có quyền truy cập');
        	echo json_encode($data); return false;
        }

        $info_user = $this->user_model->get_info($data->id_user);
        if(!$info_user){
        	$data = array('status' => '401', 'msg' => 'Không tồn tại nhân viên này');
        	echo json_encode($data); return false;
        }

        $info_detail = $this->campaign_detail_model->get_info($data->id_detail);
        if(!$info_user){
        	$data = array('status' => '401', 'msg' => 'Không tồn tại chiến dịch này');
        	echo json_encode($data); return false;
        }

        $ins = array(
        	'id_campaign'	=> $data->id_detail,
        	'id_user'		=> $data->id_user,
        	'n_house'		=> $data->n_house,
        	'image'			=> $data->img,
        	'reason'		=> $data->reason,
        	'created'		=> now()
        	);
        if($this->campaign_error_model->create($ins)){
        	$data = array('status' => '200', 'msg' => 'Thêm phát lỗi thành công');
        	echo json_encode($data);
        }
        else{
        	$data = array('status' => '401', 'msg' => 'Đã có lỗi xảy ra');
        	echo json_encode($data);
        }
    }
}