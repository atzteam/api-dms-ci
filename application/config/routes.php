<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['sitemap.xml'] = "sitemap/index";
$route['admin'] = "admin/home";
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['lol/(:num)'] = 'lol/id/$1';
$route['lol/50k.html'] = 'lol/k50';
$route['lol/100k.html'] = 'lol/k100';
$route['lol/500k.html'] = 'lol/k500';
$route['lol/tren-500k.html'] = 'lol/tren_k500';
$route['lol/chua-rank.html'] = 'lol/chua_rank';
$route['lol/100-tuong.html'] = 'lol/tuong_100';
$route['lol/vang.html'] = 'lol/vang';

// đột kích
$route['cf/(:num)'] = 'cf/id/$1';
$route['cf/c4.html'] = 'cf/c4';
$route['cf/sniper.html'] = 'cf/sniper';
$route['cf/zombie.html'] = 'cf/zombie';
$route['cf/can-chien.html'] = 'cf/can_chien';