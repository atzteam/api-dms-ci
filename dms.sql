-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 04:53 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dms`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `time_start` int(11) NOT NULL,
  `time_end` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`id`, `code`, `name`, `time_start`, `time_end`, `created`) VALUES
(1, 'CD0001', 'Phát tờ rơi bình thạnh', 1499884444, 1500501600, 1499885773),
(2, 'CD002', 'rảnh cho nhân viên đi dạo', 1499885773, 1500501600, 1499885773);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_detail`
--

CREATE TABLE `campaign_detail` (
  `id` int(11) NOT NULL,
  `id_campaign` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `align` varchar(5) NOT NULL,
  `n_start` int(11) NOT NULL,
  `n_end` int(11) NOT NULL,
  `long_start` varchar(255) NOT NULL,
  `lat_start` varchar(255) NOT NULL,
  `long_end` varchar(255) NOT NULL,
  `lat_end` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `campaign_detail`
--

INSERT INTO `campaign_detail` (`id`, `id_campaign`, `id_user`, `address`, `align`, `n_start`, `n_end`, `long_start`, `lat_start`, `long_end`, `lat_end`, `status`, `created`) VALUES
(1, 1, 1, 'Điện biên phủ', 'left', 1, 179, '10.798838', '106.7175703', '10.798880', '106.705350', 0, 1499886136),
(2, 1, 2, 'Điện biên phủ', 'right', 50, 250, '10.79623', '106.702587', '10.796232', '106.702587', 0, 1499886136),
(3, 2, 1, 'Điện biên phủ', 'left', 1, 179, '10.898838', '106.8175703', '10.798880', '106.705350', 0, 1499886136);

-- --------------------------------------------------------

--
-- Table structure for table `campaign_error`
--

CREATE TABLE `campaign_error` (
  `id` int(11) NOT NULL,
  `id_campaign` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `n_house` varchar(255) NOT NULL,
  `lon` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `campaign_error`
--

INSERT INTO `campaign_error` (`id`, `id_campaign`, `id_user`, `n_house`, `lon`, `lat`, `image`, `reason`, `created`) VALUES
(1, 1, 1, '20/3', '', '', 'idnvdsafdsf.jpg', 'thằng này nó đang chịch nên méo nhận', 1499892108);

-- --------------------------------------------------------

--
-- Table structure for table `checkin`
--

CREATE TABLE `checkin` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_campaign` int(11) NOT NULL,
  `content` text NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lng` varchar(255) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `checkin`
--

INSERT INTO `checkin` (`id`, `id_user`, `id_campaign`, `content`, `lat`, `lng`, `created`) VALUES
(1, 1, 0, 'dasdad', '10.43243', '106.4324', 1499921881),
(2, 1, 0, 'dasdad', '10.43250', '106.4324', 1499921891),
(3, 1, 0, 'dasdad', '10.43260', '106.4324', 1499921897),
(4, 2, 0, 'dasdad', '10.43220', '106.4324', 1499921904),
(5, 2, 0, 'dasdad', '10.43230', '106.4324', 1499921907),
(6, 1, 0, 'dasdad', '10.43290', '106.4324', 1499921915),
(7, 1, 0, 'dasdad', '10.43290', '106.4324', 1499921915),
(8, 1, 0, 'dasdad', '10.43290', '106.4324', 1499924563),
(9, 2, 0, 'vhjj', '10.8324076', '106.7689754', 1499927537),
(10, 2, 0, 'hunggg', '10.8323789', '106.7689178', 1499928115),
(11, 1, 0, 'di hoc', '10.8268573', '106.7210106', 1499936597),
(12, 1, 0, 'di boi', '10.8238024', '106.719306', 1499939494);

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `content` text NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id`, `id_user`, `content`, `created`) VALUES
(45, 2, 'b4dc700ab9efb3d84ef1757f0cbabbe2', 1500084633),
(53, 1, 'b4e391d002be8497decb3415287052e3', 1500171388);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `fullname`, `password`, `admin`, `created`) VALUES
(1, 'anhtd', 'Trần Đức Anh', 'e10adc3949ba59abbe56e057f20f883e', 1, 1499880935),
(2, 'hungpv', 'Phan Văn Hùng', 'e10adc3949ba59abbe56e057f20f883e', 0, 1499880935);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_detail`
--
ALTER TABLE `campaign_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaign_error`
--
ALTER TABLE `campaign_error`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `campaign_detail`
--
ALTER TABLE `campaign_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `campaign_error`
--
ALTER TABLE `campaign_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
